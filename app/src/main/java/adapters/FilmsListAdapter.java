package adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tahirietrit.meeting8.R;

import java.util.ArrayList;

import model.Date;
import model.Film;

/**
 * Created by macb on 11/10/17.
 */

public class FilmsListAdapter extends BaseAdapter {
    ArrayList<Film> films = new ArrayList<>();
    LayoutInflater inflater;

    public FilmsListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return films.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.days_list_item, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Film film = films.get(i);

        holder.title.setText(film.getTitle());
        Picasso.
                with(view.getContext()).
                load(film.getThumb_img()).
                into(holder.thumb);
        return view;
    }

    public void setDates(ArrayList<Film> films) {
        this.films = films;
        notifyDataSetChanged();
    }

    class ViewHolder {
        ImageView thumb;
        TextView title;

        public ViewHolder(View v) {
            thumb = (ImageView) v.findViewById(R.id.thumb);
            title = (TextView) v.findViewById(R.id.title);
        }
    }
}

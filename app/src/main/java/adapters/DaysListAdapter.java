package adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tahirietrit.meeting8.R;

import java.util.ArrayList;

import model.Date;

/**
 * Created by macb on 11/10/17.
 */

public class DaysListAdapter extends BaseAdapter {
    ArrayList<Date> dates = new ArrayList<>();
    LayoutInflater inflater;

    public DaysListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.days_list_item, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Date date = dates.get(i);

        holder.dayTextView.setText(date.getDay());
        holder.dateTextView.setText(date.getDate());

        return view;
    }

    public void setDates(ArrayList<Date> dates) {
        this.dates = dates;
        notifyDataSetChanged();
    }

    class ViewHolder {
        TextView dayTextView;
        TextView dateTextView;

        public ViewHolder(View v) {
            dateTextView = (TextView) v.findViewById(R.id.date_textview);
            dayTextView = (TextView) v.findViewById(R.id.day_textview);
        }
    }
}

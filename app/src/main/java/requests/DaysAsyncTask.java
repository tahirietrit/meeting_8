package requests;

import android.os.AsyncTask;

import java.io.IOException;

import api.ApiService;
import callbacks.DaysResponseCallback;
import model.DateResponse;

/**
 * Created by macb on 09/10/17.
 */

public class DaysAsyncTask extends AsyncTask<String, String, String> {

    DaysResponseCallback callback;

    public DaysAsyncTask(DaysResponseCallback callback){
        this.callback = callback;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            return ApiService.get(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        DateResponse dateResponse = new DateResponse(s);
        callback.onDaysResponse(dateResponse.getDates());
    }
}

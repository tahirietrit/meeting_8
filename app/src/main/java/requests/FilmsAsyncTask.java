package requests;

import android.os.AsyncTask;

import java.io.IOException;

import api.ApiService;
import callbacks.FilmsCallback;
import model.FilmsResponse;

/**
 * Created by macb on 11/10/17.
 */

public class FilmsAsyncTask extends AsyncTask<String, String, String> {
    FilmsCallback callback;
    public FilmsAsyncTask(FilmsCallback callback){
        this.callback = callback;
    }
    @Override
    protected String doInBackground(String... strings) {
        try {
            return ApiService.get(strings[0]);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        FilmsResponse response = new FilmsResponse(s);
        callback.onFilmsCallback(response.getFilms());
    }
}

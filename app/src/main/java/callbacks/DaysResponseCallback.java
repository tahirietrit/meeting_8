package callbacks;

import java.util.ArrayList;

import model.Date;

/**
 * Created by macb on 09/10/17.
 */

public interface DaysResponseCallback {
    void onDaysResponse(ArrayList<Date> dates);
}

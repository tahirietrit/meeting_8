package callbacks;

import java.util.ArrayList;

import model.Film;

/**
 * Created by macb on 11/10/17.
 */

public interface FilmsCallback {
    void onFilmsCallback(ArrayList<Film> films);
}

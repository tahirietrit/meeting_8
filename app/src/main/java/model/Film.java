package model;

import org.json.JSONObject;

/**
 * Created by macb on 11/10/17.
 */

public class Film {
    String id;
    String title;
    String thumb_img;
    String large_img;
    String category;
    String director;
    String time;
    String venue;
    public Film(JSONObject jsonObject){
        setId(jsonObject.optString("id"));
        setTitle(jsonObject.optString("title"));
        setThumb_img(jsonObject.optString("thumb_img"));
        setLarge_img(jsonObject.optString("large_img"));
        setCategory(jsonObject.optString("category"));
        setDirector(jsonObject.optString("director"));
        setTime(jsonObject.optString("time"));
        setVenue(jsonObject.optString("venue"));
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLarge_img() {
        return large_img;
    }

    public void setLarge_img(String large_img) {
        this.large_img = large_img;
    }

    public String getThumb_img() {
        return thumb_img;
    }

    public void setThumb_img(String thumb_img) {
        this.thumb_img = thumb_img;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }
}

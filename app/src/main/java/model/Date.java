package model;

import org.json.JSONObject;

/**
 * Created by macb on 09/10/17.
 */

public class Date {

    String day;
    String date;

    public Date(JSONObject jsonObject){
        setDate(jsonObject.optString("date"));
        setDay(jsonObject.optString("day"));
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }
}

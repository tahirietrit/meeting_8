package model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by macb on 11/10/17.
 */

public class FilmsResponse {

    ArrayList<Film> films = new ArrayList<>();

    public FilmsResponse(String s){
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); i++) {
                films.add(new Film(jsonArray.optJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Film> getFilms() {
        return films;
    }
}

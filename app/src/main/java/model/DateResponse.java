package model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by macb on 09/10/17.
 */

public class DateResponse {
    ArrayList<Date> dates = new ArrayList<>();

    public DateResponse(String s){
        try {
            JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); i++) {
                dates.add(new Date(jsonArray.optJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Date> getDates() {
        return dates;
    }
}

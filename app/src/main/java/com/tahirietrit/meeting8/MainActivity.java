package com.tahirietrit.meeting8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import adapters.DaysListAdapter;
import callbacks.DaysResponseCallback;
import model.Date;
import requests.DaysAsyncTask;

public class MainActivity extends AppCompatActivity  implements DaysResponseCallback {
    public static final String DAYS_URL = "http://api.appsix.us/prifilm/?action=days";
//    http://api.appsix.us/prifilm/?action=films&day=28.04.2014

    ListView daysListview;
    DaysListAdapter daysListAdapter;
    ArrayList<Date> dates = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new DaysAsyncTask(this).execute(DAYS_URL);

        daysListview = (ListView) findViewById(R.id.days_listview);
        daysListAdapter = new DaysListAdapter(getLayoutInflater());
        daysListview.setAdapter(daysListAdapter);
        daysListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), FilmsActivity.class);
                intent.putExtra("date", dates.get(i).getDate());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDaysResponse(ArrayList<Date> dates) {
        this.dates = dates;
        daysListAdapter.setDates(dates);
    }
}

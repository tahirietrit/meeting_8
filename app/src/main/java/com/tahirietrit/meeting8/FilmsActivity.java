package com.tahirietrit.meeting8;

import android.app.Activity;
import android.os.Bundle;

import java.util.ArrayList;

import callbacks.FilmsCallback;
import model.Film;
import requests.FilmsAsyncTask;

/**
 * Created by macb on 11/10/17.
 */

public class FilmsActivity extends Activity implements FilmsCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String date = getIntent().getStringExtra("date");
        new FilmsAsyncTask(this).execute(getUrl(date));

    }

    private String getUrl(String date) {
        return "http://api.appsix.us/prifilm/?action=films&day=" + date;
    }

    @Override
    public void onFilmsCallback(ArrayList<Film> films) {
        System.out.println("films response "+ films.size());
    }
}
